package dao;

import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class UserDao implements Repository<User, Integer> {
    private static UserDao userDao;

    private UserDao() {
    }

    public static UserDao getInstance() {
        if (userDao == null) {
            return userDao = new UserDao();
        } else {
            return userDao;
        }
    }

    @Override
    public void insert(User user) {
        final String SQL_QUERY = "INSERT INTO users (name, password, national_code, status, budget) VALUES (?,?,?,?,?)";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setInt(3, user.getNationalCode());
            preparedStatement.setBoolean(4, user.isStatus());
            preparedStatement.setDouble(5, user.getBudget());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User searchById(Integer id) {
        final String SQL_QUERY = "SELECT * FROM users WHERE id=?";
        User user = new User();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getInt(1));
                user.setName(resultSet.getString(2));
                user.setPassword(resultSet.getString(3));
                user.setNationalCode(resultSet.getInt(4));
                user.setStatus(resultSet.getBoolean(5));
                user.setBudget(resultSet.getDouble(6));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (user.getId() == id) {
            return user;
        } else {
            return null;
        }
    }

    public User searchByNationalCode(int nationalCode) {
        final String SQL_QUERY = "SELECT * FROM users WHERE national_code=?";
        User user = new User();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, nationalCode);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getInt(1));
                user.setName(resultSet.getString(2));
                user.setPassword(resultSet.getString(3));
                user.setNationalCode(resultSet.getInt(4));
                user.setStatus(resultSet.getBoolean(5));
                user.setBudget(resultSet.getDouble(6));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (user.getNationalCode() == nationalCode) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    public int update(User user) {
        final String SQL_QUERY = "UPDATE users SET (name, password, national_code, status, budget) VALUES (?,?,?,?,?)";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setInt(3, user.getNationalCode());
            preparedStatement.setBoolean(4, user.isStatus());
            preparedStatement.setDouble(5, user.getBudget());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void updateBudget(Integer id, double amount) {
        final String SQL_QUERY = "UPDATE users SET budget =? WHERE id =?";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setDouble(1, amount);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        final String SQL_QUERY = "DELETE FROM users WHERE id=?";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Collection<User> selectAll() {
        final String SQL_QUERY = "SELECT * FROM users";
        List<User> userList = null;
        User user = new User();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            userList = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery(SQL_QUERY);
            while (resultSet.next()) {
                user.setId(resultSet.getInt(1));
                user.setName(resultSet.getString(2));
                user.setPassword(resultSet.getString(3));
                user.setNationalCode(resultSet.getInt(4));
                user.setStatus(resultSet.getBoolean(5));
                user.setBudget(resultSet.getDouble(6));
                userList.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }
}
