package dao;

import java.util.Collection;


public interface Repository<T, Integer> {
    void insert(T t);

    T searchById(Integer id);

    int update(T t);

    void delete(Integer id);

    Collection<T> selectAll();

}