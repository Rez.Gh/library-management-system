package dao;

import model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class BookDao implements Repository<Book, Integer> {
    private static BookDao bookDao;

    private BookDao() {
    }

    public static BookDao getInstance() {
        if (bookDao == null) {
            return bookDao = new BookDao();
        } else {
            return bookDao;
        }
    }

    @Override
    public void insert(Book book) {
        final String SQL_QUERY = "INSERT INTO books (title, author, subject, isbn, status, price, penalty)" +
                "VALUES (?,?,?,?,?,?,?)";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setString(3, book.getSubject());
            preparedStatement.setLong(4, book.getIsbn());
            preparedStatement.setBoolean(5, book.isStatus());
            preparedStatement.setDouble(6, book.getPrice());
            preparedStatement.setDouble(7, book.getPenalty());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Book searchById(Integer id) {
        final String SQL_QUERY = "SELECT * FROM books WHERE id=?";
        Book book = new Book();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAuthor(resultSet.getString(3));
                book.setSubject(resultSet.getString(4));
                book.setIsbn(resultSet.getLong(5));
                book.setStatus(resultSet.getBoolean(6));
                book.setPrice(resultSet.getDouble(7));
                book.setPenalty(resultSet.getDouble(8));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (id == book.getId()) {
            return book;
        } else {
            return null;
        }
    }

    @Override
    public int update(Book book) {
        final String SQL_QUERY = "UPDATE books SET (title, author, subject, isbn, status, price, penalty)" +
                "VALUES (?,?,?,?,?,?,?)";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setString(3, book.getSubject());
            preparedStatement.setLong(4, book.getIsbn());
            preparedStatement.setBoolean(5, book.isStatus());
            preparedStatement.setDouble(6, book.getPrice());
            preparedStatement.setDouble(7, book.getPenalty());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void updateStatus(int bookId, boolean status) {
        final String SQL_QUERY = "UPDATE books SET status=? WHERE id=?";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setBoolean(1, status);
            preparedStatement.setInt(2, bookId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        final String SQL_QUERY = "DELETE FROM books WHERE id=?";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Collection<Book> selectAll() {
        final String SQL_QUERY = "SELECT * FROM books";
        List<Book> bookList = null;
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            bookList = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery(SQL_QUERY);
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAuthor(resultSet.getString(3));
                book.setSubject(resultSet.getString(4));
                book.setStatus(resultSet.getBoolean(6));
                book.setPrice(resultSet.getDouble(7));
                bookList.add(book);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookList;
    }
}
