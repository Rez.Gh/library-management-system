package dao;

import model.BorrowInfo;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class BorrowDao implements Repository<BorrowInfo, Integer> {
    private static BorrowDao borrowDao;

    private BorrowDao() {
    }

    public static BorrowDao getInstance() {
        if (borrowDao == null) {
            return borrowDao = new BorrowDao();
        } else {
            return borrowDao;
        }
    }


    @Override
    public void insert(BorrowInfo borrowInfo) {
        final String SQL_QUERY = "INSERT INTO borrows (user_id, book_id, start, end)" +
                "VALUES (?,?,?,?)";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, borrowInfo.getUserId());
            preparedStatement.setInt(2, borrowInfo.getBookId());
            preparedStatement.setDate(3, borrowInfo.getStartBorrow());
            preparedStatement.setDate(4, borrowInfo.getEnd());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public BorrowInfo searchById(Integer id) {
        final String SQL_QUERY = "SELECT * FROM borrows WHERE id=?";
        BorrowInfo borrowInfo = new BorrowInfo();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                borrowInfo.setId(resultSet.getInt(1));
                borrowInfo.setUserId(resultSet.getInt(2));
                borrowInfo.setBookId(resultSet.getInt(3));
                borrowInfo.setStartBorrow(resultSet.getDate(4));
                borrowInfo.setEnd(resultSet.getDate(5));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (id == borrowInfo.getId()) {
            return borrowInfo;
        } else {
            return null;
        }
    }

    public BorrowInfo searchByBookId(Integer id){
        final String SQL_QUERY = "SELECT * FROM borrows WHERE book_id=?";
        BorrowInfo borrowInfo = new BorrowInfo();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                borrowInfo.setId(resultSet.getInt(1));
                borrowInfo.setUserId(resultSet.getInt(2));
                borrowInfo.setBookId(resultSet.getInt(3));
                borrowInfo.setStartBorrow(resultSet.getDate(4));
                borrowInfo.setEnd(resultSet.getDate(5));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (id == borrowInfo.getBookId()) {
            return borrowInfo;
        } else {
            return null;
        }
    }

    @Override
    public int update(BorrowInfo borrowInfo) {
        final String SQL_QUERY = "UPDATE borrows SET (user_id, book_id, start, end)" +
                "VALUES (?,?,?,?,)";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1, borrowInfo.getUserId());
            preparedStatement.setInt(2, borrowInfo.getBookId());
            preparedStatement.setDate(3, borrowInfo.getStartBorrow());
            preparedStatement.setDate(4, borrowInfo.getEnd());
           preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void delete(Integer id) {
        final String SQL_QUERY = "DELETE FROM borrows WHERE id=?";
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Collection<BorrowInfo> selectAll() {
        final String SQL_QUERY = "SELECT * FROM borrows";
        User user = null;
        List<BorrowInfo> borrowInfoList =  new ArrayList<>();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                BorrowInfo borrowInfo = new BorrowInfo();
                borrowInfo.setId(resultSet.getInt(1));
                borrowInfo.setUserId(resultSet.getInt(2));
                borrowInfo.setBookId(resultSet.getInt(3));
                borrowInfo.setStartBorrow(resultSet.getDate(4));
                borrowInfo.setEnd(resultSet.getDate(5));
                borrowInfoList.add(borrowInfo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (borrowInfoList == null){
           return borrowInfoList = new ArrayList<>();
        }else {
            return borrowInfoList;
        }
    }

    public Collection<BorrowInfo> selectAll(Integer userId) {
        final String SQL_QUERY = "SELECT * FROM borrows WHERE user_id=?";
        User user = null;
        List<BorrowInfo> borrowInfoList =  new ArrayList<>();
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY)) {
            preparedStatement.setInt(1,userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                BorrowInfo borrowInfo = new BorrowInfo();
                borrowInfo.setId(resultSet.getInt(1));
                borrowInfo.setUserId(resultSet.getInt(2));
                borrowInfo.setBookId(resultSet.getInt(3));
                borrowInfo.setStartBorrow(resultSet.getDate(4));
                borrowInfo.setEnd(resultSet.getDate(5));
                borrowInfoList.add(borrowInfo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (borrowInfoList == null){
            return borrowInfoList = new ArrayList<>();
        }else {
            return borrowInfoList;
        }
    }
}
