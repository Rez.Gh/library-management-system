package gui;

import dao.BookDao;
import dao.BorrowDao;
import dao.UserDao;
import model.BorrowInfo;
import model.User;
import service.LibraryService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Vector;


public class ProfileFrame extends JFrame {

    private JButton btnLibraryBooks;
    private JButton btnIncreaseBudget;
    private JButton btnReturn;
    private JButton btnExit;
    private JScrollPane jScrollPane2;
    private JTable tblBorrowedBooks;
    private JLabel txtBudget;
    private JLabel txtName;
    private JLabel txtNationalCode;
    private JLabel txtStatus;
    private JLabel txtTitleBooksList;
    private JLabel txtCurrentDate;
    private int profileId;
    private LocalDate currentDate;
    private DefaultTableModel model;


    public ProfileFrame() {
        initComponents();
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    public static void main(String[] args) {


        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }


        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ProfileFrame().setVisible(true);
            }
        });
    }

    public int getUserID(Integer id) {
        return profileId = id;
    }

    private void initComponents() {

        btnLibraryBooks = new JButton();
        txtCurrentDate = new JLabel();
        txtName = new JLabel();
        txtBudget = new JLabel();
        txtNationalCode = new JLabel();
        txtTitleBooksList = new JLabel();
        btnLibraryBooks = new JButton();
        btnExit = new JButton();
        btnIncreaseBudget = new JButton();
        jScrollPane2 = new JScrollPane();
        tblBorrowedBooks = new JTable();
        txtStatus = new JLabel();
        btnReturn = new JButton();
        model = new DefaultTableModel(new String[]{"Id", "Title", "Author", "Price", "Start Date", "Return Date"}, 0);

        setTitle("Profile");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setBackground(new Color(255, 255, 255));
        getContentPane().setPreferredSize(new java.awt.Dimension(800, 600));
        setLocation(new Point(300, 90));
        setMinimumSize(new Dimension(800, 600));
        setName("Profile"); // NOI18N
        setResizable(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        txtCurrentDate.setFont(new Font("Titillium Web", Font.BOLD, 14));
        txtCurrentDate.setHorizontalAlignment(SwingConstants.CENTER);
        txtCurrentDate.setVerticalAlignment(SwingConstants.TOP);


        txtName.setFont(new Font("Titillium Web", Font.BOLD, 14)); // NOI18N
        txtName.setHorizontalAlignment(SwingConstants.LEFT);

        txtBudget.setFont(new Font("Titillium Web", Font.BOLD, 14)); // NOI18N
        txtBudget.setHorizontalAlignment(SwingConstants.LEFT);

        txtNationalCode.setFont(new Font("Titillium Web", Font.BOLD, 14)); // NOI18N
        txtNationalCode.setHorizontalAlignment(SwingConstants.LEFT);

        txtTitleBooksList.setFont(new Font("Titillium Web", 1, 18)); // NOI18N
        txtTitleBooksList.setHorizontalAlignment(SwingConstants.CENTER);
        txtTitleBooksList.setText("Borrowed Books :");
        txtTitleBooksList.setToolTipText("");

        btnLibraryBooks.setBackground(new Color(0, 102, 102));
        btnLibraryBooks.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        btnLibraryBooks.setText("Books of Library");
        btnLibraryBooks.setBorder(null);
        btnLibraryBooks.setMaximumSize(new Dimension(120, 40));
        btnLibraryBooks.setMinimumSize(new Dimension(120, 40));
        btnLibraryBooks.setPreferredSize(new Dimension(120, 40));
        btnLibraryBooks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnLibraryBooksActionPerformed(evt);
            }
        });

        btnExit.setBackground(new Color(204, 0, 0));
        btnExit.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        btnExit.setText("Exit");
        btnExit.setBorder(null);
        btnExit.setMaximumSize(new Dimension(120, 40));
        btnExit.setMinimumSize(new Dimension(120, 40));
        btnExit.setPreferredSize(new Dimension(120, 40));
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        btnIncreaseBudget.setBackground(new Color(61, 160, 255));
        btnIncreaseBudget.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        btnIncreaseBudget.setText("Increase Budget");
        btnIncreaseBudget.setBorderPainted(false);
        btnIncreaseBudget.setMaximumSize(new Dimension(120, 40));
        btnIncreaseBudget.setMinimumSize(new Dimension(120, 40));
        btnIncreaseBudget.setPreferredSize(new Dimension(120, 40));
        btnIncreaseBudget.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnIncreaseBudgetActionPerformed(evt);
            }
        });

        tblBorrowedBooks.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        tblBorrowedBooks.setModel(new DefaultTableModel(new Object[][]{}, new String[]{}));
        tblBorrowedBooks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblBorrowedBooks.setFillsViewportHeight(true);
        tblBorrowedBooks.setBackground(new Color(255, 255, 255));
        tblBorrowedBooks.setRowHeight(25);
        tblBorrowedBooks.setShowHorizontalLines(true);
        int[] columnWidth = new int[]{50, 350, 200, 200, 200, 200};
        tblBorrowedBooks.setModel(model);
        for (int i = 0; i < tblBorrowedBooks.getColumnModel().getColumnCount(); i++) {
            tblBorrowedBooks.getColumnModel().getColumn(i).setPreferredWidth(columnWidth[i]);
        }
        jScrollPane2.setViewportView(tblBorrowedBooks);
        jScrollPane2.setPreferredSize(new Dimension(1200, 300));

        txtStatus.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        txtStatus.setHorizontalAlignment(SwingConstants.LEFT);

        btnReturn.setBackground(new Color(51, 0, 255));
        btnReturn.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        btnReturn.setText("Return");
        btnReturn.setBorder(null);
        btnReturn.setMaximumSize(new Dimension(180, 30));
        btnReturn.setMinimumSize(new Dimension(180, 30));
        btnReturn.setPreferredSize(new Dimension(180, 30));
        btnReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnReturnActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(209, 209, 209)
                                .addGap(197, 197, 197))
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(158, 158, 158)
                                                .addComponent(txtTitleBooksList, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(22, 22, 22)
                                                .addComponent(btnReturn, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(231, 231, 231)
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                                        .addComponent(btnLibraryBooks, GroupLayout.Alignment.CENTER, 180, 400, 400)
                                                        .addComponent(btnExit, GroupLayout.Alignment.CENTER, 180, 400, 400)
                                                        .addComponent(btnIncreaseBudget, GroupLayout.Alignment.CENTER, 180, 400, 400))
                                                .addGap(182, 182, 182))
                                        .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                .addGap(39, 39, 39)
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                                        .addComponent(txtNationalCode, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(txtName, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE))
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                                        .addComponent(txtBudget, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(txtStatus, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)))
                                                        .addComponent(jScrollPane2))))
                                .addGap(36, 36, 36))
                        .addGroup(layout.createSequentialGroup()
                                .addGap(178, 178, 178)
                                .addComponent(txtCurrentDate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(193, 193, 193))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txtCurrentDate, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtName, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtStatus, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                                .addGap(27, 27, 27)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtNationalCode, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtBudget, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtTitleBooksList, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnReturn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
                                .addGap(45, 45, 45)
                                .addComponent(btnLibraryBooks, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnIncreaseBudget, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnExit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34))
        );
        pack();
    }

    private void refresh() {
        dispose();
        ProfileFrame profileFrame = new ProfileFrame();
        profileFrame.getUserID(profileId);
        profileFrame.setVisible(true);
    }

    private void formWindowOpened(java.awt.event.WindowEvent evt) {
        currentDate = LocalDate.now();
        txtCurrentDate.setText(currentDate.format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
        UserDao userDao = UserDao.getInstance();
        User user = userDao.searchById(profileId);
        String userStatus = user.isStatus() ? "Active" : "Suspended";
        txtName.setText("Name : " + user.getName());
        txtNationalCode.setText("National Code : " + user.getNationalCode());
        txtBudget.setText("Budget : " + user.getBudget());
        txtStatus.setText("User Status : " + userStatus);

        tblBorrowedBooks.setModel(model);
        ArrayList<Vector> borrowedBooks = (ArrayList<Vector>) LibraryService.fillBorrowedBooksTable(profileId);
        if (!borrowedBooks.isEmpty()) {
            for (int i = 0; i < borrowedBooks.size(); i++) {
                model.addRow(borrowedBooks.get(i));
            }
            tblBorrowedBooks.setModel(model);
        }
    }

    private void btnLibraryBooksActionPerformed(ActionEvent evt) {
        LibraryMainFrame libraryMainFrame = new LibraryMainFrame();
        libraryMainFrame.getUserID(profileId);
        libraryMainFrame.setVisible(rootPaneCheckingEnabled);
        dispose();
    }

    private void btnIncreaseBudgetActionPerformed(ActionEvent evt) {
        IncreaseBudgetFrame budgetFrame = new IncreaseBudgetFrame();
        budgetFrame.getUserID(profileId);
        budgetFrame.setVisible(rootPaneCheckingEnabled);
        dispose();
    }

    private void btnReturnActionPerformed(ActionEvent evt) {
        BookDao bookDao = BookDao.getInstance();
        BorrowDao borrowDao = BorrowDao.getInstance();
        int rowSelected = tblBorrowedBooks.getSelectedRow();
        if (rowSelected < 0) {
            UIManager UI = new UIManager();
            UI.put("OptionPane.background", new Color(255, 255, 255));
            UI.put("OptionPane.label", Color.GREEN);

            JOptionPane.showMessageDialog(null, "No books selected");
        } else {
            String idString = (String) tblBorrowedBooks.getValueAt(rowSelected, 0);
            int bookId = Integer.parseInt(idString);
            BorrowInfo borrowInfo = borrowDao.searchByBookId(bookId);
            if (LibraryService.hasPenalty(currentDate, borrowInfo)) {
                int delayDays = currentDate.getDayOfMonth() - borrowInfo.getEnd().toLocalDate().getDayOfMonth();
                double penalty = bookDao.searchById(bookId).getPenalty() * delayDays;
                LibraryService.decreaseBudget(profileId, penalty);
                JOptionPane.showMessageDialog(null, String.format("You returned the book %s day late.%nPenalty : %s",
                        delayDays, penalty));
                LibraryService.returnBook(borrowInfo);
            } else {
                LibraryService.returnBook(borrowInfo);
                JOptionPane.showMessageDialog(null, "Thank you for timely returning the book");
            }
            refresh();
        }
    }

    private void btnExitActionPerformed(ActionEvent evt) {
        dispose();
    }

}