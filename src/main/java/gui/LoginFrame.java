package gui;

import dao.UserDao;
import model.User;
import service.LibraryService;

import javax.swing.*;


public class LoginFrame extends JFrame {

    private JButton btnLogin;
    private JTextField inputPassword;
    private JTextField nationalInput;
    private JLabel txtNationalCode;
    private JLabel txtPassword;
    private JLabel txtTitle;

    public LoginFrame() {
        initComponents();
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    public static void main(String[] args) {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
    }

    private void initComponents() {

        btnLogin = new JButton();
        inputPassword = new JTextField();
        txtPassword = new JLabel();
        nationalInput = new JTextField();
        txtNationalCode = new JLabel();
        txtTitle = new JLabel();

        setTitle("Login");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setPreferredSize(new java.awt.Dimension(800, 600));
        setLocation(new java.awt.Point(300, 90));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("LoginFrame"); // NOI18N
        setResizable(true);

        btnLogin.setBackground(new java.awt.Color(0, 102, 102));
        btnLogin.setFont(new java.awt.Font("Titillium Web", 1, 14)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.setBorder(null);
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        inputPassword.setFont(new java.awt.Font("Titillium Web", 1, 12)); // NOI18N
        inputPassword.setHorizontalAlignment(JTextField.LEFT);

        txtPassword.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtPassword.setHorizontalAlignment(SwingConstants.CENTER);
        txtPassword.setText("Password :");

        nationalInput.setFont(new java.awt.Font("Titillium Web", 0, 14)); // NOI18N
        nationalInput.setHorizontalAlignment(JTextField.LEFT);

        txtNationalCode.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtNationalCode.setHorizontalAlignment(SwingConstants.CENTER);
        txtNationalCode.setText("National Code :");

        txtTitle.setFont(new java.awt.Font("Titillium Web", 1, 18)); // NOI18N
        txtTitle.setHorizontalAlignment(SwingConstants.CENTER);
        txtTitle.setText("Enter your National code and password");
        txtTitle.setPreferredSize(new java.awt.Dimension(254, 50));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(146, 146, 146)
                                .addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addGap(134, 134, 134))
                        .addGroup(layout.createSequentialGroup()
                                .addGap(157, 157, 157)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(btnLogin, GroupLayout.Alignment.CENTER, 180, 400, 400)
                                        .addComponent(inputPassword, GroupLayout.Alignment.CENTER, 180, 600, 600)
                                        .addComponent(nationalInput, GroupLayout.Alignment.CENTER, 180, 600, 600))
                                .addGap(144, 144, 144))
                        .addGroup(layout.createSequentialGroup()
                                .addGap(244, 244, 244)
                                .addComponent(txtNationalCode, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(217, 217, 217))
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(222, 222, 222)
                                .addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addGap(199, 199, 199))
                        .addGroup(layout.createSequentialGroup()
                                .addGap(237, 237, 237)
                                .addGap(204, 204, 204))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                                .addGap(56, 56, 56)
                                .addComponent(txtNationalCode, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nationalInput, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                                .addGap(25, 25, 25)
                                .addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(inputPassword, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(46, Short.MAX_VALUE))
        );

        pack();
    }

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {
        if (nationalInput.getText().equals("")
                && inputPassword.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please national code and password");

        } else {
            try {
                User user = new User(Integer.valueOf(nationalInput.getText()), inputPassword.getText());
                UserDao userDao = UserDao.getInstance();
                boolean isLogin = LibraryService.authentication(user);
                if (isLogin) {
                    ProfileFrame profileFrame = new ProfileFrame();
                    profileFrame.getUserID(userDao.searchByNationalCode(user.getNationalCode()).getId());
                    profileFrame.setVisible(rootPaneCheckingEnabled);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "National code or password incorrect");
                    nationalInput.setText("");
                    inputPassword.setText("");
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Please enter only number for national code");
            }

        }

    }

}

