package gui;

import dao.BookDao;
import dao.UserDao;
import model.Book;
import model.BorrowInfo;
import service.LibraryService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class LibraryMainFrame extends JFrame {

    private JButton btnBack;
    private JButton btnBorrow;
    private JScrollPane jScrollPane1;
    private JTable tblBooks;
    private JLabel txtTitle;
    private LocalDate currentDate;
    private LocalDate returnedDate;
    private int profileId;
    private DefaultTableModel model;


    public LibraryMainFrame() {
        initComponents();
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    public static void main(String[] args) {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LibraryMainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LibraryMainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LibraryMainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LibraryMainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LibraryMainFrame().setVisible(true);
            }
        });
    }

    public int getUserID(Integer id) {
        return profileId = id;
    }

    private void initComponents() {

        txtTitle = new JLabel();
        jScrollPane1 = new JScrollPane();
        tblBooks = new JTable();
        btnBorrow = new JButton();
        btnBack = new JButton();
        model = new DefaultTableModel(new String[]{"Id", "Title", "Author", "Subject", "Status", "Price"}, 0);

        setTitle("Books of Library");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setBackground(new Color(255, 255, 255));
        getContentPane().setPreferredSize(new Dimension(800, 600));
        setLocation(new Point(300, 90));
        setMinimumSize(new Dimension(800, 600));
        setName("LibraryMainFrame"); // NOI18N
        setResizable(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        txtTitle.setFont(new Font("Titillium Web", 1, 18)); // NOI18N
        txtTitle.setHorizontalAlignment(SwingConstants.CENTER);
        txtTitle.setText("Books of Library");

        tblBooks.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        tblBooks.setModel(new DefaultTableModel(new Object[][]{}, new String[]{}));
        tblBooks.setRowSelectionAllowed(true);
        tblBooks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblBooks.setFillsViewportHeight(true);
        tblBooks.setBackground(new Color(255, 255, 255));
        tblBooks.getTableHeader().setBackground(new Color(255, 255, 255));
        tblBooks.setRowHeight(25);
        tblBooks.setShowHorizontalLines(true);
        int[] columnWidth = new int[]{50, 350, 200, 200, 200, 200};
        tblBooks.setModel(model);
        for (int i = 0; i < tblBooks.getColumnModel().getColumnCount(); i++) {
            tblBooks.getColumnModel().getColumn(i).setPreferredWidth(columnWidth[i]);
        }
//        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
//        cellRenderer.setHorizontalAlignment(JLabel.LEFT);
//        tblBooks.getColumnModel().getColumn(0).setCellRenderer(cellRenderer);

        jScrollPane1.setViewportView(tblBooks);
        jScrollPane1.setPreferredSize(new Dimension(1200, 600));

        btnBorrow.setBackground(new Color(51, 0, 255));
        btnBorrow.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        btnBorrow.setText("Borrow");
        btnBorrow.setBorder(null);
        btnBorrow.setMaximumSize(new Dimension(120, 30));
        btnBorrow.setMinimumSize(new Dimension(120, 30));
        btnBorrow.setPreferredSize(new Dimension(120, 30));
        btnBorrow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrowActionPerformed(evt);
            }
        });

        btnBack.setBackground(new Color(204, 0, 0));
        btnBack.setFont(new Font("Titillium Web", 1, 14)); // NOI18N
        btnBack.setText("Back");
        btnBack.setBorder(null);
        btnBack.setMaximumSize(new Dimension(120, 30));
        btnBack.setMinimumSize(new Dimension(120, 30));
        btnBack.setPreferredSize(new Dimension(120, 30));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(btnBorrow, GroupLayout.Alignment.CENTER, 180, 400, 400)
                        .addComponent(btnBack, GroupLayout.Alignment.CENTER, 180, 200, 200)
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(210, 210, 210)
                                .addComponent(txtTitle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(207, 207, 207))
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(256, 256, 256)

                                .addGap(235, 235, 235))
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jScrollPane1)
                                .addGap(43, 43, 43))
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(275, 275, 275)
                                .addGap(248, 248, 248))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 348, GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(btnBorrow, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnBack, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }

    private void formWindowOpened(java.awt.event.WindowEvent evt) {

        List<Book> bookList = (ArrayList<Book>) LibraryService.fillLibraryBooksTable();

        for (int i = 0; i < bookList.size(); i++) {
            Book book = bookList.get(i);
            String[] bookRow = new String[6];
            bookRow[0] = String.valueOf(book.getId());
            bookRow[1] = book.getTitle();
            bookRow[2] = book.getAuthor();
            bookRow[3] = book.getSubject();
            bookRow[4] = book.isStatus() ? "Available" : "Unavailable";
            bookRow[5] = String.valueOf(book.getPrice());
            model.addRow(bookRow);
        }
        tblBooks.setModel(model);
    }

    private void btnBorrowActionPerformed(java.awt.event.ActionEvent evt) {
        if (tblBooks.getSelectedRow() >= 0) {
            int row = tblBooks.getSelectedRow();
            String idString = (String) tblBooks.getValueAt(row, 0);
            int bookId = Integer.parseInt(idString);
            double budget = UserDao.getInstance().searchById(profileId).getBudget();
            double price = BookDao.getInstance().searchById(bookId).getPrice();

            if (!LibraryService.isAvailable(bookId)) {
                JOptionPane.showMessageDialog(null, "This book unavailable");
            } else if (budget < price) {
                JOptionPane.showMessageDialog(null, "Your budget is not enough");
            } else {
                currentDate = LocalDate.now();
                returnedDate = currentDate.plusWeeks(1);
                BorrowInfo borrowInfo = new BorrowInfo(Date.valueOf(currentDate), Date.valueOf(returnedDate), profileId, bookId);
                LibraryService.borrowBook(borrowInfo);
                String bookTitle = BookDao.getInstance().searchById(bookId).getTitle();
                double fee = BookDao.getInstance().searchById(bookId).getPrice();
                LibraryService.decreaseBudget(profileId, fee);
                JOptionPane.showMessageDialog(null, String.format("You are  borrowed %s for a week%nWeekly fee : %s",
                        bookTitle, fee));
                refresh();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Select a book");
        }

    }

    private void refresh() {
        dispose();
        LibraryMainFrame libraryMainFrame = new LibraryMainFrame();
        libraryMainFrame.getUserID(profileId);
        libraryMainFrame.setVisible(true);
    }

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {
        ProfileFrame frame = new ProfileFrame();
        frame.getUserID(profileId);
        frame.setVisible(rootPaneCheckingEnabled);
        dispose();
    }

}
