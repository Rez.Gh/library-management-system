package gui;

import dao.UserDao;
import model.User;
import service.LibraryService;

import javax.swing.*;

public class IncreaseBudgetFrame extends JFrame {

    private JButton btnIncrease;
    private JTextField inputBudget;
    private JLabel txtCurrentBudget;
    private JButton btnBack;
    private int profileId;

    public IncreaseBudgetFrame() {
        initComponents();
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    public static void main(String[] args) {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IncreaseBudgetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IncreaseBudgetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IncreaseBudgetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IncreaseBudgetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IncreaseBudgetFrame().setVisible(true);
            }
        });
    }

    public int getUserID(Integer id) {
        return profileId = id;
    }

    private void initComponents() {

        txtCurrentBudget = new JLabel();
        inputBudget = new JTextField();
        btnIncrease = new JButton();
        btnBack = new JButton();

        setTitle("Increase Budget");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setBackground(new java.awt.Color(255, 255, 255));
        setLocation(new java.awt.Point(300, 90));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("IncreaseBudgetFrame"); // NOI18N
        setResizable(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        txtCurrentBudget.setFont(new java.awt.Font("Titillium Web", 1, 18)); // NOI18N
        txtCurrentBudget.setHorizontalAlignment(SwingConstants.CENTER);

        inputBudget.setFont(new java.awt.Font("Titillium Web", 1, 14)); // NOI18N
        inputBudget.setHorizontalAlignment(JTextField.LEFT);

        btnIncrease.setBackground(new java.awt.Color(51, 0, 255));
        btnIncrease.setFont(new java.awt.Font("Titillium Web", 1, 14)); // NOI18N
        btnIncrease.setText("Increse Budget");
        btnIncrease.setBorder(null);
        btnBack.setMaximumSize(new java.awt.Dimension(120, 40));
        btnBack.setMinimumSize(new java.awt.Dimension(120, 40));
        btnBack.setPreferredSize(new java.awt.Dimension(120, 40));
        btnIncrease.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncreaseActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(204, 0, 0));
        btnBack.setFont(new java.awt.Font("Titillium Web", 1, 14)); // NOI18N
        btnBack.setText("Back");
        btnBack.setBorder(null);
        btnBack.setMaximumSize(new java.awt.Dimension(120, 30));
        btnBack.setMinimumSize(new java.awt.Dimension(120, 30));
        btnBack.setPreferredSize(new java.awt.Dimension(120, 30));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(168, 168, 168)
                                .addGap(166, 166, 166))
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)

                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(400, 400, 400)
                                                .addGap(400, 400, 400))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(400, 400, 400)
                                                .addComponent(inputBudget)))
                                .addGap(400, 400, 400))
                        .addComponent(txtCurrentBudget, GroupLayout.Alignment.CENTER, 180, 400, 400)
                        .addComponent(btnIncrease, GroupLayout.Alignment.CENTER, 180, 400, 400)
                        .addComponent(btnBack, GroupLayout.Alignment.CENTER, 180, 200, 200)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(txtCurrentBudget, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                .addGap(99, 99, 99)
                                .addComponent(inputBudget, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                .addGap(62, 62, 62)
                                .addComponent(btnIncrease, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnBack, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(60, Short.MAX_VALUE))

        );

        pack();
    }

    private void formWindowOpened(java.awt.event.WindowEvent evt) {
        UserDao userDao = UserDao.getInstance();
        User user = userDao.searchById(profileId);
        txtCurrentBudget.setText("My Budget : " + user.getBudget());
    }

    private void btnIncreaseActionPerformed(java.awt.event.ActionEvent evt) {
        if (inputBudget.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Enter amount");
        } else {
            double amount = Double.parseDouble(inputBudget.getText());
            if (amount <= 0) {
                JOptionPane.showMessageDialog(null, "Please enter a correct amount");
                inputBudget.setText("");
            } else {
                LibraryService.increaseBudget(profileId, amount);
                JOptionPane.showMessageDialog(null, String.format("Budget increased %s $", amount));
                ProfileFrame profileFrame = new ProfileFrame();
                profileFrame.getUserID(profileId);
                profileFrame.setVisible(rootPaneCheckingEnabled);
                dispose();
            }
        }
    }

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {
        ProfileFrame frame = new ProfileFrame();
        frame.getUserID(profileId);
        frame.setVisible(rootPaneCheckingEnabled);
        dispose();
    }
}

