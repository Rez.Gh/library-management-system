package gui;

import dao.UserDao;
import model.User;
import service.LibraryService;

import javax.swing.*;

public class RegisterFrame extends JFrame {

    private JButton btnRegister;
    private JTextField inputNationalCode;
    private JTextField inputPassword;
    private JTextField inputName;
    private JLabel txtName;
    private JLabel txtNationalCode;
    private JLabel txtPassword;
    private JLabel txtTitle;

    public RegisterFrame() {
        initComponents();
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    public static void main(String[] args) {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegisterFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegisterFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegisterFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegisterFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }


        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegisterFrame().setVisible(true);
            }
        });
    }

    private void initComponents() {

        txtTitle = new JLabel();
        txtName = new JLabel();
        inputName = new JTextField();
        txtPassword = new JLabel();
        inputPassword = new JTextField();
        btnRegister = new JButton();
        inputNationalCode = new JTextField();
        txtNationalCode = new JLabel();

        setTitle("Register");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setPreferredSize(new java.awt.Dimension(800, 600));
        setLocation(new java.awt.Point(300, 90));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("RegisterFrame");
        setResizable(true);

        txtTitle.setFont(new java.awt.Font("Titillium Web", 1, 18)); // NOI18N
        txtTitle.setHorizontalAlignment(SwingConstants.CENTER);
        txtTitle.setText("Join us and borrow book");
        txtTitle.setPreferredSize(new java.awt.Dimension(254, 50));

        txtName.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtName.setHorizontalAlignment(SwingConstants.CENTER);
        txtName.setText("Name :");

        inputName.setFont(new java.awt.Font("Titillium Web", 0, 14)); // NOI18N
        inputName.setHorizontalAlignment(JTextField.LEFT);

        txtPassword.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtPassword.setHorizontalAlignment(SwingConstants.CENTER);
        txtPassword.setText("Password :");

        inputPassword.setFont(new java.awt.Font("Titillium Web", 1, 12)); // NOI18N
        inputPassword.setHorizontalAlignment(JTextField.LEFT);

        btnRegister.setBackground(new java.awt.Color(51, 0, 255));
        btnRegister.setPreferredSize(new java.awt.Dimension(300, 40));
        btnRegister.setFont(new java.awt.Font("Titillium Web", 1, 14)); // NOI18N
        btnRegister.setText("Register");
        btnRegister.setBorder(null);
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        inputNationalCode.setFont(new java.awt.Font("Titillium Web", 0, 14)); // NOI18N
        inputNationalCode.setHorizontalAlignment(JTextField.LEFT);

        txtNationalCode.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtNationalCode.setHorizontalAlignment(SwingConstants.CENTER);
        txtNationalCode.setText("National Code :");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(199, 199, 199)
                                .addComponent(txtTitle, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                                .addGap(153, 153, 153))
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(165, 165, 165)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(87, 87, 87)
                                                .addComponent(txtName, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                                .addGap(73, 73, 73))
                                        .addComponent(inputPassword, GroupLayout.Alignment.CENTER, 180, 600, 600)
                                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                                .addGap(65, 65, 65)
                                                .addComponent(txtPassword, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                                                .addGap(55, 55, 55))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(80, 80, 80)
                                                .addGap(60, 60, 60))
                                        .addComponent(inputNationalCode, GroupLayout.Alignment.CENTER, 180, 600, 600)
                                        .addComponent(inputName, GroupLayout.Alignment.CENTER, 180, 600, 600)
                                        .addComponent(btnRegister, GroupLayout.Alignment.CENTER, 180, 400, 400)
                                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                                .addGap(100, 100, 100)
                                                .addComponent(txtNationalCode, GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                                                .addGap(93, 93, 93)))
                                .addGap(136, 136, 136))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(txtName, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                                .addGap(22, 22, 22)
                                .addComponent(txtNationalCode, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(inputNationalCode, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                                .addGap(13, 13, 13)
                                .addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(inputPassword, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(btnRegister, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                                .addGap(47, 47, 47))
        );

        pack();
    }

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {
        if (inputName.getText().equals("") && inputNationalCode.getText().equals("")
                && inputPassword.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter Name, National code and Password");
        } else {
            User user = new User(inputName.getText(), Integer.valueOf(inputNationalCode.getText()), inputPassword.getText());
            boolean isRegister = LibraryService.register(user);
            if (isRegister) {
                ProfileFrame profileFrame = new ProfileFrame();
                UserDao userDao = UserDao.getInstance();
                profileFrame.getUserID(userDao.searchByNationalCode(user.getNationalCode()).getId());
                profileFrame.setVisible(rootPaneCheckingEnabled);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Already registered with this national code.");
                inputName.setText("");
                inputNationalCode.setText("");
                inputPassword.setText("");
            }
        }
    }

}

