package gui;

import javax.swing.*;

/**
 * @author Reza
 */
public class MainFrame extends JFrame {

    private JButton btnLogin;
    private JButton btnRegister;
    private JLabel txtTitle;

    public MainFrame() {
        initComponents();
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    public static void main(String[] args) {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | UnsupportedLookAndFeelException ex) {

            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                new MainFrame().setVisible(true);
            }
        });
    }

    private void initComponents() {

        txtTitle = new JLabel();
        btnLogin = new JButton();
        btnRegister = new JButton();

        setTitle("Library Management");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setPreferredSize(new java.awt.Dimension(800, 600));
        setLocation(new java.awt.Point(300, 90));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("mainFrame");
        setResizable(true);


        txtTitle.setText("Library Management");
        txtTitle.setFont(new java.awt.Font("Titillium Web", 1, 24));
        txtTitle.setHorizontalAlignment(SwingConstants.CENTER);
        txtTitle.setMaximumSize(new java.awt.Dimension(214, 50));
        txtTitle.setMinimumSize(new java.awt.Dimension(214, 50));
        txtTitle.setPreferredSize(new java.awt.Dimension(120, 60));

        btnLogin.setText("Login");
        btnLogin.setBackground(new java.awt.Color(0, 102, 102));
        btnLogin.setFont(new java.awt.Font("Titillium Web", 1, 18)); // NOI18N
        btnLogin.setBorder(null);
        btnLogin.setMaximumSize(new java.awt.Dimension(80, 40));
        btnLogin.setMinimumSize(new java.awt.Dimension(80, 40));
        btnLogin.setPreferredSize(new java.awt.Dimension(80, 40));
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        btnRegister.setText("Register");
        btnRegister.setBackground(new java.awt.Color(51, 0, 255));
        btnRegister.setFont(new java.awt.Font("Titillium Web", 1, 18)); // NOI18N
        btnRegister.setBorder(null);
        btnRegister.setPreferredSize(new java.awt.Dimension(380, 40));
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });


        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(141, 141, 141)
                                .addComponent(txtTitle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(139, 139, 139))
                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                .addGap(179, 179, 179)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(btnRegister, GroupLayout.Alignment.CENTER, 180, 600, 600)
                                        .addComponent(btnLogin, GroupLayout.Alignment.CENTER, 180, 600, 600))
                                .addGap(184, 184, 184))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 140, Short.MAX_VALUE)
                                .addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(31, 31, 31)
                                .addComponent(btnRegister, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(106, 106, 106))
        );

        pack();
    }

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {
        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(rootPaneCheckingEnabled);
        dispose();
    }

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {
        RegisterFrame registerFrame = new RegisterFrame();
        registerFrame.setVisible(rootPaneCheckingEnabled);
        dispose();
    }
}
