package service;

import dao.BookDao;
import dao.BorrowDao;
import dao.UserDao;
import model.Book;
import model.BorrowInfo;
import model.User;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;


public final class LibraryService {

    private LibraryService() {
    }

    public static boolean register(User user) {
        UserDao userDao = UserDao.getInstance();
        if (userDao.searchByNationalCode(user.getNationalCode()) == null) {
            userDao.insert(user);
            return true;
        } else {
            return false;
        }
    }

    public static boolean authentication(User user) {
        UserDao userDao = UserDao.getInstance();
        if (userDao.searchByNationalCode(user.getNationalCode()) != null &&
                userDao.searchByNationalCode(user.getNationalCode()).getPassword().equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }

    public static void increaseBudget(int id, double amount) {
        UserDao userDao = UserDao.getInstance();
        amount = userDao.searchById(id).getBudget() + amount;
        userDao.updateBudget(id, amount);
    }

    public static void decreaseBudget(int id, double amount) {
        UserDao userDao = UserDao.getInstance();
        amount = userDao.searchById(id).getBudget() - amount;
        userDao.updateBudget(id, amount);
    }

    public static Collection<Book> fillLibraryBooksTable() {
        BookDao bookDao = BookDao.getInstance();
        return bookDao.selectAll();
    }

    public static Collection fillBorrowedBooksTable(int userId) {
        BorrowDao borrowDao = BorrowDao.getInstance();
        ArrayList<BorrowInfo> borrowInfos = (ArrayList<BorrowInfo>) borrowDao.selectAll(userId);
        List<String> userBorrowedBook = null;
        List<Vector> userBorrowedBooks = new ArrayList<>();
        BookDao bookDao = BookDao.getInstance();
        for (int i = 0; i < borrowInfos.size(); i++) {
            BorrowInfo borrowInfo = borrowInfos.get(i);
            if (userId == borrowInfo.getUserId()) {
                userBorrowedBook = new Vector<>();
                userBorrowedBook.add(String.valueOf(borrowInfo.getBookId()));
                userBorrowedBook.add(bookDao.searchById(borrowInfo.getBookId()).getTitle());
                userBorrowedBook.add(bookDao.searchById(borrowInfo.getBookId()).getAuthor());
                userBorrowedBook.add(String.valueOf(bookDao.searchById(borrowInfo.getBookId()).getPrice()));
                userBorrowedBook.add(String.valueOf(borrowInfo.getStartBorrow()));
                userBorrowedBook.add(String.valueOf(borrowInfo.getEnd()));
            }
            userBorrowedBooks.add((Vector) userBorrowedBook);
        }
        return userBorrowedBooks;
    }

    public static boolean isAvailable(int boolId) {
        BookDao bookDao = BookDao.getInstance();
        boolean status = bookDao.searchById(boolId).isStatus();
        if (status) {
            return true;
        } else {
            return false;
        }
    }

    public static void borrowBook(BorrowInfo borrowInfo) {
        BookDao bookDao = BookDao.getInstance();
        BorrowDao borrowDao = BorrowDao.getInstance();
        borrowDao.insert(borrowInfo);
        int bookId = borrowInfo.getBookId();
        bookDao.updateStatus(bookId, false);
    }

    public static boolean hasPenalty(LocalDate currentDate, BorrowInfo borrowInfo) {
        if (currentDate.isAfter(borrowInfo.getEnd().toLocalDate())) {
            return true;
        }
        return false;
    }

    public static void returnBook(BorrowInfo borrowInfo) {
        BookDao bookDao = BookDao.getInstance();
        BorrowDao borrowDao = BorrowDao.getInstance();
        borrowDao.delete(borrowInfo.getId());
        int bookId = borrowInfo.getBookId();
        bookDao.updateStatus(bookId, true);
    }
}

