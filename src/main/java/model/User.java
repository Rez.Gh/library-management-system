package model;


public class User extends Person {
    private String password;
    private boolean status;
    private double budget;

    public User() {
    }

    public User(int nationalCode, String password) {
        super(nationalCode);
        this.password = password;
    }

    public User(String name, int nationalCode, String password) {
        super(name,nationalCode);
        this.password = password;
        this.status = true;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return  super.toString();
    }

}

