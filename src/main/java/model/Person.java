package model;


public class Person extends BaseEntity {
    private String name;
    private int nationalCode;

    public Person() {
    }

    public Person(int nationalCode) {
        this.nationalCode = nationalCode;
    }

    public Person(int id, String name, int nationalCode) {
        super(id);
        this.name = name;
        this.nationalCode = nationalCode;
    }

    public Person(String name, int nationalCode) {
        super();
        this.name = name;
        this.nationalCode = nationalCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(int nationalCode) {
        this.nationalCode = nationalCode;
    }

    @Override
    public String toString() {
        return "Person{" +
                "fullName='" + name + '\'' +
                ", nationalCode='" + nationalCode + '\'' +
                '}';
    }

}
