package model;

import java.sql.Date;

public class BorrowInfo extends BaseEntity {
    private Date startBorrow;
    private Date end;
    private int userId;
    private int bookId;

    public BorrowInfo() {
    }

    public BorrowInfo(Date startBorrow, Date end, int userId, int bookId) {
        this.startBorrow = startBorrow;
        this.end = end;
        this.userId = userId;
        this.bookId = bookId;
    }

    public Date getStartBorrow() {
        return startBorrow;
    }

    public void setStartBorrow(Date startBorrow) {
        this.startBorrow = startBorrow;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

}
